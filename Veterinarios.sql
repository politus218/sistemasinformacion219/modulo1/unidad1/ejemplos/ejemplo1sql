﻿-- Ejemplo de creación de una base de datos de veterinarios
-- Tiene 3 tablas

DROP DATABASE IF EXISTS b20190528;
CREATE DATABASE b20190528;
USE b20190528;

/*
  Creando la tabla de animales con sus campos

*/


CREATE TABLE animales(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  raza varchar(100),
  fechaNac date,
  PRIMARY KEY(id)
  );

CREATE TABLE veterinarios (
  cod int AUTO_INCREMENT,
  nombre varchar(100),
  especialidad varchar(100),
  PRIMARY KEY(cod)
  );

CREATE TABLE curan (
  idanimal int,
  codvet int,
  fecha date,
  PRIMARY KEY(idanimal,codvet,fecha), -- creando la clave principal
  UNIQUE KEY (idanimal),
  CONSTRAINT fkcurananimal FOREIGN KEY (idanimal) REFERENCES animales (id),
  CONSTRAINT fkcuranveterinarios FOREIGN KEY (codvet) REFERENCES veterinarios (cod)
  );


-- insertar un resgistro

INSERT INTO animales (nombre, raza, fechaNac)  VALUES 
  ('jorge','bulldog','2000/2/1'),
  ('ana','caniche','2002/1/');

-- listar

  SELECT * FROM animales;

